EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "lun. 30 mars 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 7450 3500 1    60   ~ 0
Vin
Text Label 7850 3500 1    60   ~ 0
IOREF
Text Label 7400 4550 0    60   ~ 0
Abort
Text Label 7400 4750 0    60   ~ 0
Resume
Text Label 7400 4950 0    60   ~ 0
A4(SDA)
Text Label 7400 5050 0    60   ~ 0
A5(SCL)
Text Label 9050 5050 0    60   ~ 0
0(RX)
Text Label 9050 4850 0    60   ~ 0
StepX
Text Label 9050 4950 0    60   ~ 0
1(TX)
Text Label 9050 4750 0    60   ~ 0
StepY
Text Label 9050 4650 0    60   ~ 0
StepZ
Text Label 9050 4550 0    60   ~ 0
DirX
Text Label 9050 4450 0    60   ~ 0
DirY
Text Label 9050 4350 0    60   ~ 0
DirZ
Text Label 9050 4150 0    60   ~ 0
ENABLE
Text Label 9050 4050 0    60   ~ 0
LimitX
Text Label 9050 3950 0    60   ~ 0
LimitY
Text Label 9050 3850 0    60   ~ 0
LimitZ
Text Label 9050 3750 0    60   ~ 0
SpnEn
Text Label 9050 3650 0    60   ~ 0
SpnDir
Text Label 9050 3450 0    60   ~ 0
AREF
Text Label 9050 3350 0    60   ~ 0
A4(SDA)2
Text Label 9050 3250 0    60   ~ 0
A5(SCL)2
$Comp
L Connector_Generic:Conn_01x08 P1
U 1 1 56D70129
P 8100 3950
F 0 "P1" H 8100 4400 50  0000 C CNN
F 1 "Power" V 8200 3950 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 8250 3950 20  0000 C CNN
F 3 "" H 8100 3950 50  0000 C CNN
	1    8100 3950
	1    0    0    -1  
$EndComp
Text Label 7150 3850 0    60   ~ 0
Reset
$Comp
L power:+3.3V #PWR01
U 1 1 56D70538
P 7650 3500
F 0 "#PWR01" H 7650 3350 50  0001 C CNN
F 1 "+3.3V" V 7650 3750 50  0000 C CNN
F 2 "" H 7650 3500 50  0000 C CNN
F 3 "" H 7650 3500 50  0000 C CNN
	1    7650 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 56D707BB
P 7550 3400
F 0 "#PWR02" H 7550 3250 50  0001 C CNN
F 1 "+5V" V 7550 3600 50  0000 C CNN
F 2 "" H 7550 3400 50  0000 C CNN
F 3 "" H 7550 3400 50  0000 C CNN
	1    7550 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 56D70CC2
P 7800 5200
F 0 "#PWR03" H 7800 4950 50  0001 C CNN
F 1 "GND" H 7800 5050 50  0000 C CNN
F 2 "" H 7800 5200 50  0000 C CNN
F 3 "" H 7800 5200 50  0000 C CNN
	1    7800 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 56D70CFF
P 8800 5200
F 0 "#PWR04" H 8800 4950 50  0001 C CNN
F 1 "GND" H 8800 5050 50  0000 C CNN
F 2 "" H 8800 5200 50  0000 C CNN
F 3 "" H 8800 5200 50  0000 C CNN
	1    8800 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 P2
U 1 1 56D70DD8
P 8100 4750
F 0 "P2" H 8100 4350 50  0000 C CNN
F 1 "Analog" V 8200 4750 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x06" V 8250 4800 20  0000 C CNN
F 3 "" H 8100 4750 50  0000 C CNN
	1    8100 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P4
U 1 1 56D7164F
P 8500 4650
F 0 "P4" H 8500 4150 50  0000 C CNN
F 1 "Digital" V 8600 4650 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 8650 4600 20  0000 C CNN
F 3 "" H 8500 4650 50  0000 C CNN
	1    8500 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7850 3500 7850 3750
Wire Wire Line
	7850 3750 7900 3750
Wire Wire Line
	7900 3950 7650 3950
Wire Wire Line
	7900 4050 7550 4050
Wire Wire Line
	7900 4350 7450 4350
Wire Wire Line
	7900 4150 7800 4150
Wire Wire Line
	7900 4250 7800 4250
Connection ~ 7800 4250
Wire Wire Line
	7450 4350 7450 3500
Wire Wire Line
	7550 4050 7550 3600
Wire Wire Line
	7650 3950 7650 3700
Wire Wire Line
	7900 4550 7400 4550
Wire Wire Line
	7900 4650 7400 4650
Wire Wire Line
	7900 4750 7400 4750
Wire Wire Line
	7900 4850 7400 4850
Wire Wire Line
	7900 4950 7400 4950
Wire Wire Line
	7900 5050 7400 5050
Wire Wire Line
	8700 4150 9050 4150
Wire Wire Line
	8700 4050 9050 4050
Wire Wire Line
	8700 3950 9050 3950
Wire Wire Line
	8700 3850 9050 3850
Wire Wire Line
	8700 3750 9050 3750
Wire Wire Line
	8700 3650 9050 3650
Wire Wire Line
	8700 3450 9050 3450
Wire Wire Line
	8700 3350 9050 3350
Wire Wire Line
	8700 3250 9050 3250
Wire Wire Line
	8700 5050 9050 5050
Wire Wire Line
	8700 4950 9050 4950
Wire Wire Line
	8700 4850 9050 4850
Wire Wire Line
	8700 4750 9050 4750
Wire Wire Line
	8700 4650 9050 4650
Wire Wire Line
	8700 4550 9050 4550
Wire Wire Line
	8700 4450 9050 4450
Wire Wire Line
	8700 4350 9050 4350
Wire Wire Line
	8700 3550 8800 3550
Wire Wire Line
	8800 3550 8800 5200
Wire Wire Line
	7800 4150 7800 4250
Wire Wire Line
	7800 4250 7800 5150
Wire Notes Line
	7000 2550 7000 5500
Wire Wire Line
	7900 3850 7150 3850
Text Notes 8200 3650 0    60   ~ 0
1
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 5DC71DAC
P 6050 3700
F 0 "J2" H 5968 3175 50  0000 C CNN
F 1 "Conn_01x06" H 5968 3266 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 6050 3700 50  0001 C CNN
F 3 "~" H 6050 3700 50  0001 C CNN
	1    6050 3700
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5DC72A9A
P 5000 4850
F 0 "J3" H 4918 4325 50  0000 C CNN
F 1 "Conn_01x06" H 4918 4416 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 5000 4850 50  0001 C CNN
F 3 "~" H 5000 4850 50  0001 C CNN
	1    5000 4850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 5DC73003
P 5000 3700
F 0 "J1" H 4918 3175 50  0000 C CNN
F 1 "Conn_01x06" H 4918 3266 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 5000 3700 50  0001 C CNN
F 3 "~" H 5000 3700 50  0001 C CNN
	1    5000 3700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5DC742FC
P 5400 4000
F 0 "#PWR05" H 5400 3750 50  0001 C CNN
F 1 "GND" H 5405 3827 50  0000 C CNN
F 2 "" H 5400 4000 50  0001 C CNN
F 3 "" H 5400 4000 50  0001 C CNN
	1    5400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3900 5400 3900
Wire Wire Line
	5400 3900 5400 4000
Connection ~ 5400 3900
Wire Wire Line
	5200 3700 5400 3700
Connection ~ 5400 3700
Wire Wire Line
	5400 3700 5400 3900
Wire Wire Line
	5200 3500 5400 3500
Wire Wire Line
	5400 3500 5400 3700
$Comp
L power:GND #PWR06
U 1 1 5DC78A4D
P 6450 4000
F 0 "#PWR06" H 6450 3750 50  0001 C CNN
F 1 "GND" H 6455 3827 50  0000 C CNN
F 2 "" H 6450 4000 50  0001 C CNN
F 3 "" H 6450 4000 50  0001 C CNN
	1    6450 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3900 6450 3900
Wire Wire Line
	6450 3900 6450 4000
Connection ~ 6450 3900
Wire Wire Line
	6250 3700 6450 3700
Connection ~ 6450 3700
Wire Wire Line
	6450 3700 6450 3900
Wire Wire Line
	6250 3500 6450 3500
Wire Wire Line
	6450 3500 6450 3700
$Comp
L power:GND #PWR07
U 1 1 5DC79C5A
P 5400 5150
F 0 "#PWR07" H 5400 4900 50  0001 C CNN
F 1 "GND" H 5405 4977 50  0000 C CNN
F 2 "" H 5400 5150 50  0001 C CNN
F 3 "" H 5400 5150 50  0001 C CNN
	1    5400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5050 5400 5050
Wire Wire Line
	5400 5050 5400 5150
Connection ~ 5400 5050
Wire Wire Line
	5200 4850 5400 4850
Connection ~ 5400 4850
Wire Wire Line
	5400 4850 5400 5050
Wire Wire Line
	5200 4650 5400 4650
Wire Wire Line
	5400 4650 5400 4850
Wire Wire Line
	5500 3600 5200 3600
Wire Wire Line
	6250 3400 6550 3400
Wire Wire Line
	6550 3600 6250 3600
Wire Wire Line
	6250 3800 6550 3800
Wire Wire Line
	5200 4550 5500 4550
Wire Wire Line
	5500 4750 5200 4750
Wire Wire Line
	5200 4950 5500 4950
Text Label 5500 3800 0    50   ~ 0
ENABLE
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DCCA732
P 7250 3100
F 0 "#FLG0101" H 7250 3175 50  0001 C CNN
F 1 "PWR_FLAG" H 7250 3273 50  0000 C CNN
F 2 "" H 7250 3100 50  0001 C CNN
F 3 "~" H 7250 3100 50  0001 C CNN
	1    7250 3100
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DCCAA98
P 7200 3450
F 0 "#FLG0102" H 7200 3525 50  0001 C CNN
F 1 "PWR_FLAG" H 7200 3623 50  0000 C CNN
F 2 "" H 7200 3450 50  0001 C CNN
F 3 "~" H 7200 3450 50  0001 C CNN
	1    7200 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3450 7200 3700
Wire Wire Line
	7200 3700 7650 3700
Connection ~ 7650 3700
Wire Wire Line
	7650 3700 7650 3500
Wire Wire Line
	7250 3600 7550 3600
Wire Wire Line
	7250 3100 7250 3600
Connection ~ 7550 3600
Wire Wire Line
	7550 3600 7550 3400
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5DCD50DD
P 7300 5300
F 0 "#FLG0103" H 7300 5375 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 5473 50  0000 C CNN
F 2 "" H 7300 5300 50  0001 C CNN
F 3 "~" H 7300 5300 50  0001 C CNN
	1    7300 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 5300 7300 5400
Wire Wire Line
	7300 5400 7600 5400
Wire Wire Line
	7600 5400 7600 5150
Wire Wire Line
	7600 5150 7800 5150
Connection ~ 7800 5150
Wire Wire Line
	7800 5150 7800 5200
$Comp
L Connector_Generic:Conn_01x13 J5
U 1 1 5DCDCC7D
P 3900 4050
F 0 "J5" H 3818 3225 50  0000 C CNN
F 1 "Conn_01x13" H 3818 3316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 3900 4050 50  0001 C CNN
F 3 "~" H 3900 4050 50  0001 C CNN
	1    3900 4050
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x13 J4
U 1 1 5DCDE01D
P 3250 4050
F 0 "J4" H 3168 3225 50  0000 C CNN
F 1 "Conn_01x13" H 3168 3316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x13_P2.54mm_Vertical" H 3250 4050 50  0001 C CNN
F 3 "~" H 3250 4050 50  0001 C CNN
	1    3250 4050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5DCDEFED
P 3600 4800
F 0 "#PWR0101" H 3600 4550 50  0001 C CNN
F 1 "GND" H 3605 4627 50  0000 C CNN
F 2 "" H 3600 4800 50  0001 C CNN
F 3 "" H 3600 4800 50  0001 C CNN
	1    3600 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3450 3600 3450
Wire Wire Line
	3600 3450 3600 3550
Wire Wire Line
	3450 3550 3600 3550
Connection ~ 3600 3550
Wire Wire Line
	3600 3550 3600 3650
Wire Wire Line
	3450 3650 3600 3650
Connection ~ 3600 3650
Wire Wire Line
	3600 3650 3600 3750
Wire Wire Line
	3450 3750 3600 3750
Connection ~ 3600 3750
Wire Wire Line
	3600 3750 3600 3850
Wire Wire Line
	3450 3850 3600 3850
Connection ~ 3600 3850
Wire Wire Line
	3600 3850 3600 3950
Wire Wire Line
	3450 3950 3600 3950
Connection ~ 3600 3950
Wire Wire Line
	3600 3950 3600 4050
Wire Wire Line
	3450 4050 3600 4050
Connection ~ 3600 4050
Wire Wire Line
	3600 4050 3600 4150
Wire Wire Line
	3450 4150 3600 4150
Connection ~ 3600 4150
Wire Wire Line
	3600 4150 3600 4250
Wire Wire Line
	3450 4250 3600 4250
Connection ~ 3600 4250
Wire Wire Line
	3600 4250 3600 4350
Wire Wire Line
	3450 4350 3600 4350
Connection ~ 3600 4350
Wire Wire Line
	3600 4350 3600 4450
Wire Wire Line
	3450 4450 3600 4450
Connection ~ 3600 4450
Wire Wire Line
	3600 4450 3600 4550
Wire Wire Line
	3450 4550 3600 4550
Connection ~ 3600 4550
Wire Wire Line
	3600 4550 3600 4650
Wire Wire Line
	3450 4650 3600 4650
Connection ~ 3600 4650
Wire Wire Line
	3600 4650 3600 4800
Wire Wire Line
	5200 3800 5500 3800
Wire Wire Line
	4100 3450 4250 3450
Wire Wire Line
	4100 3550 4250 3550
Wire Wire Line
	4100 3650 4250 3650
Wire Wire Line
	4100 3750 4250 3750
Wire Wire Line
	4100 3850 4250 3850
Wire Wire Line
	4100 3950 4250 3950
Wire Wire Line
	4100 4050 4250 4050
Wire Wire Line
	4100 4150 4250 4150
Wire Wire Line
	4100 4250 4250 4250
Wire Wire Line
	4100 4350 4250 4350
Wire Wire Line
	4100 4450 4250 4450
Wire Wire Line
	4100 4550 4250 4550
Wire Wire Line
	4100 4650 4250 4650
Text Label 4250 3450 0    60   ~ 0
LimitX
Text Label 4250 3650 0    60   ~ 0
LimitY
Text Label 4250 3550 0    60   ~ 0
LimitX
Text Label 4250 3750 0    60   ~ 0
LimitY
Text Label 4250 3950 0    60   ~ 0
LimitZ
Text Label 4250 3850 0    60   ~ 0
LimitZ
Text Label 6550 3800 0    50   ~ 0
ENABLE
Text Label 5500 4950 0    50   ~ 0
ENABLE
Wire Wire Line
	5200 3400 5500 3400
Text Label 5500 3600 0    60   ~ 0
DirZ
Text Label 6550 3600 0    60   ~ 0
DirY
Text Label 5500 4750 0    60   ~ 0
DirX
Text Label 5500 3400 0    60   ~ 0
StepZ
Text Label 6550 3400 0    60   ~ 0
StepY
Text Label 5500 4550 0    60   ~ 0
StepX
Wire Notes Line
	4800 5500 4800 2550
Wire Notes Line
	3000 5500 9700 5500
Text Notes 4850 2800 0    60   ~ 0
Connectors B6B-XH-A from JST\nConnect to stepper driver board\nConnects clock, enable and direction
Text Notes 3050 2800 0    60   ~ 0
Connectors for limit switches\nand extra functionalities for the cnc
Text Label 4250 4450 0    60   ~ 0
Hold
Text Label 4250 4250 0    60   ~ 0
Coolant
Text Label 4250 4050 0    60   ~ 0
SpnEn
Text Label 4250 4150 0    60   ~ 0
SpnDir
Text Label 4250 4350 0    60   ~ 0
Abort
Text Label 7400 4650 0    60   ~ 0
Hold
Text Label 4250 4550 0    60   ~ 0
Resume
Text Label 7400 4850 0    60   ~ 0
Coolant
Text Label 4250 4650 0    60   ~ 0
Reset
NoConn ~ 8700 4950
NoConn ~ 8700 5050
NoConn ~ 7900 5050
NoConn ~ 7900 4950
NoConn ~ 7900 4350
NoConn ~ 7900 3750
NoConn ~ 7900 3650
NoConn ~ 8700 3250
NoConn ~ 8700 3350
NoConn ~ 8700 3450
Wire Notes Line
	9500 5500 9500 2500
Wire Notes Line
	9500 2500 3000 2500
Wire Notes Line
	3000 2500 3000 5500
$Comp
L Connector_Generic:Conn_01x10 P3
U 1 1 56D721E0
P 8500 3650
F 0 "P3" H 8500 4200 50  0000 C CNN
F 1 "Digital" V 8600 3650 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x10" V 8650 3650 20  0000 C CNN
F 3 "" H 8500 3650 50  0000 C CNN
	1    8500 3650
	-1   0    0    -1  
$EndComp
Text Notes 7030 2800 0    60   ~ 0
Shield for Arduino that uses\nthe same pin disposition\nlike "Uno" board Rev 3.
Wire Notes Line
	9500 2850 3000 2850
$EndSCHEMATC
