# 16x16 LED matrix

This is a simple PCB made in KiCad using 4 small 8x8 LED matrices serially connected to form a 16x16 LED matrix.

Go to [this repo](https://gitlab.com/hasecilu/16x16-led-matrix)

